﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarboRouting
{
    class Program
    {

        static void Main(string[] args)
        {
            List<Node> _nodes = new List<Node>();

            Node _baseStation = new Node();
            _baseStation.X = 0;
            _baseStation.Y = 0;
            _baseStation.GatewayId = _baseStation.NodeId;
            _nodes.Add(_baseStation);

            for (int i = 0; i < 2; i++)
            {
                Node _node = new Node(0, 0, 0);
                _nodes.Add(_node);

                for (int j = 0; j < 3; j++)
                {
                    Node _childNode = new Node(0, _node.X, _node.Y);
                    _nodes.Add(_childNode);
                }
            }

            for (int i = 0; i < _nodes.Count; i++)
            {
                if (!_nodes[i].GatewayId.Equals(Guid.Empty))
                {
                    for (int j = 0; j < _nodes.Count; j++)
                    {
                        if (j != i)
                        {
                            double _distance = getDistance(_nodes[i], _nodes[j]);

                            if (_distance < Node.MaxR * 1.5)
                            {
                                if (_nodes[j].GatewayId.Equals(Guid.Empty))
                                {
                                    _nodes[j].GatewayId = _nodes[i].NodeId;
                                    _nodes[j].Hop = _nodes[i].Hop + 1;
                                    _nodes[j].Distance = _nodes[i].Distance + _distance;
                                }
                                else if (_nodes[i].Hop + 1 < _nodes[j].Hop)
                                {
                                    _nodes[j].GatewayId = _nodes[i].NodeId;
                                    _nodes[j].Hop = _nodes[i].Hop + 1;
                                    _nodes[j].Distance = _nodes[i].Distance + _distance;
                                }
                                else if (_nodes[i].Distance + _distance < _nodes[j].Distance)
                                {
                                    _nodes[j].GatewayId = _nodes[i].NodeId;
                                    _nodes[j].Hop = _nodes[i].Hop + 1;
                                    _nodes[j].Distance = _nodes[i].Distance + _distance;
                                }
                            }
                        }
                    }
                }
            }

            foreach (var _sensor in _nodes)
            {
                Console.WriteLine(_sensor.ToString());
            }

            Console.ReadLine();
        }

        static double getDistance(Node a, Node b)
        {
            double _dx = a.X - b.X;
            double _dy = a.Y - b.Y;

            double _value = _dx * _dx + _dy * _dy;
            _value = Math.Sqrt(_value);

            return _value;
        }
    }

    class Node
    {
        public const double MaxR = 10;
        private Random rand;

        public Guid NodeId { get; set; }
        public Guid GatewayId { get; set; }
        public int Hop { get; set; }

        public double X { get; set; }
        public double Y { get; set; }
        public double Distance { get; set; }

        public Node()
        {
            rand = new Random();

            NodeId = Guid.NewGuid();
            GatewayId = Guid.Empty;
            Hop = 0;
            Distance = 0;

            X = rand.NextDouble() * MaxR;
            Y = rand.NextDouble() * MaxR;
        }

        public Node(int seed, double x, double y)
        {
            rand = new Random(seed);

            NodeId = Guid.NewGuid();
            GatewayId = Guid.Empty;
            Hop = -1;

            X = x + rand.NextDouble() * MaxR;
            Y = y + rand.NextDouble() * MaxR;
        }

        public override string ToString()
        {
            string _value = string.Format(
                "Sesnor {0}: Gateway {1}, with {2} hop(s), {3} km far.",
                NodeId, GatewayId, Hop, Distance);

            return _value;
        }
    }
}
